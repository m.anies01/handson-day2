import java.util.*;

public class MyApp {
    public static void main(String[] args) {
        // Objek baru dari scanner untuk masukan nilai
        Scanner inputData   = new Scanner(System.in);
        // Membuat objek baru dari kelas Student
        Student attrStudent = new Student();

        String nama,email,phoneNumber;

        System.out.print("Ketik nama, email & No. HP: ");
        nama        = inputData.next();
        email       = inputData.next();
        phoneNumber = inputData.next();

        // memasukkan nilai nama dari input scanner ke dalam objek attrStudent ke dalam 
        // atribut yang ada di class Student
        attrStudent.name        = nama;
        attrStudent.email       = email;
        attrStudent.phoneNumber = phoneNumber;

        //Menampilkan data
        System.out.println("Student: "+attrStudent);
        System.out.println("Student.getName(): "+attrStudent.name);
        System.out.println("Student.getEmail(): "+attrStudent.email);
        System.out.println("Student.getPhoneNumber(): "+attrStudent.phoneNumber);
    }    
}
